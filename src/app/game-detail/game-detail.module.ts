import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameDetailComponent } from './game-detail.component';



@NgModule({
  declarations: [
    GameDetailComponent
  ],
  imports: [
    CommonModule
  ]
})
export class GameDetailModule { }
