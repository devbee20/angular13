import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpHeaderInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({
            setHeaders: {
              'Content-Type': 'application/json',
              'x-rapidapi-key': 'esGbwrm390mshS2BCl0RALxQRtZTp1W7sFMjsnyJlJzDXVkW0H',
              'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
            },
            setParams: {
              key: 'e40e743af2c94b0c916a8aa618fb4473',
            }
          });
        return next.handle(authReq);
    }
}